package _10_Strategy_01;

public interface Comparator {
	
	/**
	 * @param lhs left hand side.
	 * @param rhs right hand side.
	 * @return 1 when lhs > rhs, 0 when lhs == rhs, -1 when lhs < rhs.
	 */
	public int compare(Human lhs, Human rhs);
}