package iterator_if;

public interface Aggregate {
    public Iterator iterator();
}
