package _07_Builder_01;

import water.Builder;
import water.Water;

public class Director {
    private Builder builder;
    
    public Director(Builder builder){
        this.builder = builder;
    }
    
    public Water constract(){
        builder.addSolvent( 100 );
        builder.addSolute( 40 );
        builder.abandonSolution( 70 );
        builder.addSolvent( 100 );
        builder.addSolute( 15 );
        
        return builder.getResult();
    }
}
