package water;

public class SaltWaterBuilder implements Builder {
	
	private SaltWater saltWater = new SaltWater();
	
	@Override
	public void addSolute(double soluteAmount) {
		saltWater.salt += soluteAmount; 
	}

	@Override
	public void addSolvent(double solventAmount) {
		saltWater.water += solventAmount;
	}

	@Override
	public void abandonSolution(double solutionAmount) {
		double saltDelta = solutionAmount * (saltWater.salt / (saltWater.salt + saltWater.water));
		double waterDelta = solutionAmount * (saltWater.water / (saltWater.salt + saltWater.water));
		saltWater.salt -= saltDelta;
		saltWater.water -= waterDelta;
	}

	@Override
	public Water getResult() {
		return saltWater;
	}
}
