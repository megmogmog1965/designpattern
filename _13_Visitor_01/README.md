
# Visitor pattern.

## はじめに

[TechscoreのVisitor pattern](http://www.techscore.com/tech/DesignPattern/Visitor.html/) は色々説明が足りなくて混乱するので、今回は無視する。

[Gof本] と [サルでもわかる...] を参考に考えを纏めてみた。

## Visitor使いどころ

ある様々な異なるクラスからなるObject構造に対して...

* それぞれのクラスのObjectに、それぞれ関連のない (共通でない) 処理をしたい
* だけど、関連の無い処理の追加や呼出で、クラスを汚したくない
    * 共通のIFを用意するも、殆どのクラスは (関係無い処理なので) 空実装...みたいな状況の事か？

Visitorを適用すると...

* Object構造のクラスのコードに変更を加える事なく、そのObjectに関する特有の処理を追加できる
* Object構造のクラスで、関連する物だけ抜き出した処理をVisitorクラスとして纏めておける

といういいことが達成できる。 (多分).

## 使えないケース

Visitor patternは、Object構造側のクラスが変化しない場合に使えるPattern.
「Object構造は不変なんだけど、これに関わる処理がなんかちょこちょこ増える...」みたいなケースで使う.


[サルでもわかる...]:http://www.nulab.co.jp/designPatterns/designPatterns2/designPatterns2-5.html
[Gof本]:http://www.amazon.co.jp/%E3%82%AA%E3%83%96%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E6%8C%87%E5%90%91%E3%81%AB%E3%81%8A%E3%81%91%E3%82%8B%E5%86%8D%E5%88%A9%E7%94%A8%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E3%83%87%E3%82%B6%E3%82%A4%E3%83%B3%E3%83%91%E3%82%BF%E3%83%BC%E3%83%B3-%E3%82%A8%E3%83%AA%E3%83%83%E3%82%AF-%E3%82%AC%E3%83%B3%E3%83%9E/dp/4797311126/ref=pd_sim_b_5?ie=UTF8&refRID=19RYBPN8207VJ17XW78Y
