package visitor;

import element.OrderItem;
import element.OrderLine;

/**
 * 処理を表すインタフェースです。
 * 
 * visit()の引数に、OrderItemや、OrderLineが来ている事から分かるが、
 * Visitor patternは、Object構造(Element)に変更が入らないケースでのみ使える。
 * 
 * もし、Elementに変更が入ったり、新しいクラスが追加されると、全てのVisitorに多大な影響を及ぼす.
 */
public interface Visitor {

	/**
	 * OrderItemに対して処理を行います.
	 */
	void visit(OrderItem item);

	/**
	 * OrderLineに対して処理を行います.
	 */
	void visit(OrderLine line);
}
