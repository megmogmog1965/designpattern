package _09_Bridge_01;

import java.util.Arrays;

public class QuickSortImpl implements SortImpl {

	@Override
	public void sort(Object[] array) {
		// 0.1 sec.
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) { e.printStackTrace(); }
		
		Arrays.sort(array);
	}
}
