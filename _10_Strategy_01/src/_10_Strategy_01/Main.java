package _10_Strategy_01;

public class Main {
	
	public static void main(String[] args) {
		Human john = new Human("John", 170, 60, 24);
		Human taro = new Human("Taro", 163, 72, 32);
		
		SampleClass ageSampler = new SampleClass(new AgeComparator());
		SampleClass heightSampler = new SampleClass(new HeightComparator());
		
		// compare theirs age.
		System.out.println("Age:");
		print(ageSampler, john, taro);
		System.out.println("");
		
		// compare theirs height.
		System.out.println("Age:");
		print(heightSampler, john, taro);
		System.out.println("");
	}
	
	/**
	 * 表示用メソッド.
	 * 
	 * @param sampler
	 * @param lhs
	 * @param rhs
	 */
	public static void print(SampleClass sampler, Human lhs, Human rhs) {
		int result = sampler.compare(lhs, rhs);
		
		if(result > 0) {
			System.out.println(lhs.name + " > " + rhs.name);
		}
		else if(result == 0) {
			System.out.println(lhs.name + " = " + rhs.name);
		}
		else {
			System.out.println(lhs.name + " < " + rhs.name);
		}
	}
}
