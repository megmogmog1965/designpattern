package _09_Bridge_01;

public interface SortImpl {
	
	public void sort(Object[] array);
}
