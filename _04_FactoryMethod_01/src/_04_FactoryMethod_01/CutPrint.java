package _04_FactoryMethod_01;

public abstract class CutPrint {
	/**
	 * public --> protected に直した
	 */
	protected abstract void draw(Cuttable hanzai);
	
	/**
	 * public --> protected に直した
	 */
	protected abstract void cut(Cuttable hanzai);
	
	/**
	 * public --> protected に直した
	 */
	protected abstract void print(Cuttable hanzai);
	
	/**
	 * this is Factory Method for Cuttable.
	 */
	protected abstract Cuttable createCuttable();
	
	/**
	 * これがTemplate method。処理の流れを決めている。
	 * 具体的な処理の内容はSubClassで決める。
	 */
	public void createWoodCutPrint() {
		// sub-class creates a conclete cuttable.
		Cuttable hanzai = this.createCuttable();
		
		// template ordered processes.
		draw(hanzai);
		cut(hanzai);
		print(hanzai);
	}
}
