package paperworks;

public interface Cloneable {

	public Cloneable createClone();
}
