package _07_Builder_01;

import water.SaltWaterBuilder;
import water.SugarWaterBuilder;
import water.Water;

public class Main {

	public static void main(String args[]) {
		// creating salt water.
		Director saltDirector = new Director(new SaltWaterBuilder());
		Water saltWater = saltDirector.constract();
		System.out.println(saltWater);
		
		// creating sugar water.
		Director sugarDirector = new Director(new SugarWaterBuilder());
		Water sugarWater = sugarDirector.constract();
		System.out.println(sugarWater);
	}
}
