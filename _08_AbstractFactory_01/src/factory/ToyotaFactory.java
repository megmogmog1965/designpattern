package factory;

import _08_AbstractFactory_01.Handle;
import _08_AbstractFactory_01.Tire;

/**
 * トヨタのパーツを作るFactory
 * (絶対に間違った組み合わせができない)
 */
public class ToyotaFactory implements ICarPartsFactory {

	@Override
	public Tire createTire() {
		return new ToyotaTire();
	}

	@Override
	public Handle createHandle() {
		return new ToyotaHandle();
	}
}
