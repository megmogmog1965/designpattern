package factory;

import _08_AbstractFactory_01.Handle;
import _08_AbstractFactory_01.Tire;

/**
 * BMWのパーツを作るFactory
 * (絶対に間違った組み合わせができない)
 */
public class BMWFactory implements ICarPartsFactory {

	@Override
	public Tire createTire() {
		return new BMWTire();
	}

	@Override
	public Handle createHandle() {
		return new BMWHandle();
	}
}
