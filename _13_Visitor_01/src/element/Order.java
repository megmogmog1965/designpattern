package element;


/**
 * すべての要素の基本となる抽象クラスです。
 */
public abstract class Order implements Element {

//	/**
//	 * 注文を追加します.
//	 */
//	public void addOrder(Order order) {
//	}

	/**
	 * 料金を集計する抽象メソッドです.
	 */
	public abstract int getAmount();
}
