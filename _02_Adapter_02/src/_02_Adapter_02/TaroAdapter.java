package _02_Adapter_02;

public class TaroAdapter implements Chairperson {
	private Taro taro;

	public TaroAdapter(Taro taro) {
		assert taro != null;
		this.taro = taro;
	}
	
	@Override
	public void organizeClass() {
		this.taro.enjoyWithAllClassmate();
	}
}
