package factory;

import _08_AbstractFactory_01.Handle;
import _08_AbstractFactory_01.Tire;

/**
 * Abstract Factory.
 * Factory methodとの違いは、「複数の具象Objectのセット(組み合わせ)を保証すること」である
 */
public interface ICarPartsFactory {
	
	public Tire createTire();
	
	public Handle createHandle();
}
