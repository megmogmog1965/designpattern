package _09_Bridge_01;

/**
 * 実装をコンストラクタから注入する方式に変更したことで、機能(=method)追加用のサブクラスが同様の実装にアクセスできるようになっている。
 */
public class Sorter {
	
	private final SortImpl impl;
	
	public Sorter(SortImpl impl) {
		this.impl = impl;
	}
	
	public void sort(Object[] array) {
		impl.sort(array);
	}
}
