package _04_FactoryMethod_01;

public class TanakasWoodCutPrint extends CutPrint {
	@Override
	protected void draw(Cuttable hanzai) {
		System.out.println("hanzai にマジックを使って大好きな女の子の絵を描く");
	}
	
	@Override
	protected void cut(Cuttable hanzai) {
		System.out.println("彫刻刀を使って細部まで丁寧に hanzai を彫る");
	}
	
	@Override
	protected void print(Cuttable hanzai) {
		System.out.println("版画インクと馬簾を使って豪快にプリントする");
	}
	
	/**
	 * this is Factory Method for Cuttable.
	 */
	@Override
	protected Cuttable createCuttable() {
		return new Wood();
	}
}