package _03_TemplateMethod_01;

public abstract class WoodCutPrint{
	/**
	 * public --> protected に直した
	 */
	protected abstract void draw(Cuttable hanzai);
	
	/**
	 * public --> protected に直した
	 */
	protected abstract void cut(Cuttable hanzai);
	
	/**
	 * public --> protected に直した
	 */
	protected abstract void print(Cuttable hanzai);
	
	/**
	 * これがTemplate method。処理の流れを決めている。
	 * 具体的な処理の内容はSubClassで決める。
	 */
	public void createWoodCutPrint() {
		Wood hanzai = new Wood(); //Wood クラスは、Cuttable インタフェースを実装している
		draw(hanzai);
		cut(hanzai);
		print(hanzai);
	}
}
