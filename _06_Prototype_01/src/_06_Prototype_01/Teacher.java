package _06_Prototype_01;

import java.util.List;
import java.util.ArrayList;

import paperworks.Paper;

public class Teacher {
	
	public static void main(String args[]) {
		List<Paper> paperList = new ArrayList<Paper>();
		
		// 1個作る (時間掛かる...)
		Paper paper = new Paper();
		Paper crystalShaped = paper.cutCrystalShape();
		paperList.add(crystalShaped);
		
		// 後9個 複製する
		for(int i=0 ; i<9 ; i++) {
			Paper cloned = (Paper)crystalShaped.createClone();
			paperList.add(cloned);
		}
		
		assert paperList.size() == 10;
		System.out.println("全部で" + paperList.size() + "個作りました");
	}
}
