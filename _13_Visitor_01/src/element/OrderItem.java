package element;

import visitor.Visitor;

/**
 * 商品クラスです。
 */
public class OrderItem extends Order {

	/* 商品の値段です。 */
	private int price;

	/**
	 * 値段を渡してクラスを初期化します.
	 */
	public OrderItem(int price) {
		this.price = price;
	}

	/**
	 * この商品の値段を返します.
	 */
	public int getAmount() {
		return this.price;
	}

	/**
	 * 処理を受け入れます.
	 */
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}