package _10_Strategy_01;

public class SampleClass {
	
	private final Comparator comparator;
	
	public SampleClass(Comparator comparator) {
		this.comparator = comparator;
	}
	
	/**
	 * @param lhs left hand side.
	 * @param rhs right hand side.
	 * @return 1 when lhs > rhs, 0 when lhs == rhs, -1 when lhs < rhs.
	 */
	public int compare(Human lhs, Human rhs) {
		return comparator.compare(lhs, rhs);
	}
}
