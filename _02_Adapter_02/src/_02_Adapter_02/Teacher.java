package _02_Adapter_02;

public class Teacher{
    public static void main(String args[]){
    	// before.
//        Chairperson chairperson = new Taro();
    	
    	// after.
    	Chairperson chairperson = new TaroAdapter(new Taro());
    	
        chairperson.organizeClass();
    }
}
