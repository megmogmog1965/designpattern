package _05_Singleton_01;

public class RegisterNote {
	private static final RegisterNote instance = new RegisterNote();
	
	private RegisterNote() {
		// this is private constructor.
	}
	
	public static RegisterNote getInstance() {
		return instance;
	}
}
