package factory;

import _08_AbstractFactory_01.Handle;

/**
 * パッケージの外からは見えない具象クラス
 */
class ToyotaHandle implements Handle {
	
	public String toString() {
		return "トヨタのハンドル";
	}
}
