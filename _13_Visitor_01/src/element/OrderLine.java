package element;

import java.util.ArrayList;
import java.util.List;

import visitor.Visitor;

/**
 * 注文をまとめるクラスです。
 */
public class OrderLine extends Order {

	/* このクラスで管理する注文および商品のリストです。 */
	private List<Order> orders = new ArrayList<>();

	/**
	 * 商品や注文を追加します.
	 */
	public void addOrder(Order order) {
		orders.add(order);
	}

	/**
	 * 管理する注文や商品の値段を返します.
	 */
	public int getAmount() {
		int result = 0;

		for(Order order : orders) {
			result += order.getAmount();
		}

		return result;
	}

	/**
	 * 処理を受け入れます.
	 */
	public void accept(Visitor visitor) {
		visitor.visit(this);

		// 管理しているリストにも処理を渡して回る
		for(Order order : orders) {
			order.accept(visitor);
		}
	}
}