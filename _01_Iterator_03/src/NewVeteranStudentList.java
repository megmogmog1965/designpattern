
import iterator_if.Aggregate;
import iterator_if.Iterator;

import java.util.Vector;

public class NewVeteranStudentList extends NewStudentList implements Aggregate {
	
	public NewVeteranStudentList(int studentCount) {
		this.students = new Vector<Student>(studentCount);
	}
	
	@Override
	public Iterator iterator() {
		return new NewVeteranStudentListIterator(this);
	}
	
	public int size() {
		return this.students.size();
	}
}
