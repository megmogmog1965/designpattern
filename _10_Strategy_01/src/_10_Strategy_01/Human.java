package _10_Strategy_01;

public class Human {

	public final String name;
	public final int height;
	public final int weight;
	public final int age;

	public Human(String name, int height, int weight, int age) {
		this.name = name;
		this.height = height;
		this.weight = weight;
		this.age = age;
	}
}