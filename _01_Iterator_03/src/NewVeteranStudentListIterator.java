
import iterator_if.Iterator;

public class NewVeteranStudentListIterator implements Iterator {
	private NewVeteranStudentList list;
	private int index;
	
	public NewVeteranStudentListIterator(NewVeteranStudentList list) {
		assert list != null;
		this.list = list;
		this.index = 0;
	}
	
	@Override
	public boolean hasNext() {
		return this.list.size() > this.index;
	}

	@Override
	public Object next() {
		return this.list.getStudentAt(index++);
	}
}
