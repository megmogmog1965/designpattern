package visitor;

import element.OrderItem;
import element.OrderLine;

/**
 * 集計処理をするクラスです.
 * 
 * OrderItemにだけ処理をするVisitorである.
 */
public class OrderCountVisitor implements Visitor {
	
	/* 総商品数です。 */
	private int itemCount = 0;

	/**
	 * OrderItemに対する処理を実行します.
	 */
	public void visit(OrderItem item) {
		itemCount++;
	}

	/**
	 * OrderLineに対する処理を実行します.
	 */
	public void visit(OrderLine line) {
		// 特に何もしない
	}

	/**
	 * 総商品数を返します.
	 */
	public int getCount() {
		return itemCount;
	}
}