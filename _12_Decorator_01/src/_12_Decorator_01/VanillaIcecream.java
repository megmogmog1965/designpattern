package _12_Decorator_01;

public class VanillaIcecream implements Icecream {
	
    public String getName() {
        return "バニラアイスクリーム";
    }
    
    public String howSweet() {
        return "バニラ味";
    }
}
