package _08_AbstractFactory_01;

public class Car {
	
	private Tire tire;
	private Handle handle;
	
	public Car(Tire tire, Handle handle) {
		this.tire = tire;
		this.handle = handle;
	}
	
	public String toString() {
		return "この車は \"" + tire + "\" と \"" + handle + "\" で, できています。";
	}
}
