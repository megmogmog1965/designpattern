package water;

public class SugarWaterBuilder implements Builder {
	
	private SugarWater sugarWater = new SugarWater();
	
	@Override
	public void addSolute(double soluteAmount) {
		sugarWater.sugar += soluteAmount;
	}

	@Override
	public void addSolvent(double solventAmount) {
		sugarWater.water += solventAmount;
	}

	@Override
	public void abandonSolution(double solutionAmount) {
		double saltDelta = solutionAmount * (sugarWater.sugar / (sugarWater.sugar + sugarWater.water));
		double waterDelta = solutionAmount * (sugarWater.water / (sugarWater.sugar + sugarWater.water));
		sugarWater.sugar -= saltDelta;
		sugarWater.water -= waterDelta;
	}

	@Override
	public Water getResult() {
		return sugarWater;
	}
}
