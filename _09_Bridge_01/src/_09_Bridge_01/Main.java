package _09_Bridge_01;

public class Main {

	public static void main(String[] args) {
		System.out.println("=============================");
		useSorter();
		System.out.println("=============================");
		useTimerSorter();
		System.out.println("=============================");
	}
	
	public static void useSorter() {
		String[] array1 = { "Orange", "Apple", "Grape" };
		String[] array2 = { "Grape", "Orange", "Apple" };
		
		// quick sort.
		Sorter sorter = new Sorter(new QuickSortImpl());
		sorter.sort(array1);
		
		// bubble sort.
		sorter = new Sorter(new BubbleSortImpl());
		sorter.sort(array2);
		
		// print.
		printArray(array1);
		printArray(array2);
	}
	
	public static void useTimerSorter() {
		String[] array1 = { "Orange", "Apple", "Grape" };
		String[] array2 = { "Grape", "Orange", "Apple" };
		
		// quick sort.
		TimerSorter sorter = new TimerSorter(new QuickSortImpl());
		sorter.timerSort(array1);
		
		// bubble sort.
		sorter = new TimerSorter(new BubbleSortImpl());
		sorter.timerSort(array2);
		
		// print.
		printArray(array1);
		printArray(array2);
	}
	
	public static void printArray(Object[] array) {
		for(Object obj : array) {
			System.out.print(obj + ", ");
		}
		System.out.println("");
	}
}
