package element;

import visitor.Visitor;

/**
 * 処理を受け入れるインタフェースです。
 */
public interface Element {
	
	void accept(Visitor visitor);
}
