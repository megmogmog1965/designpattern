package _09_Bridge_01;

public class TimerSorter extends Sorter {

	public TimerSorter(SortImpl impl) {
		super(impl);
	}

	public void timerSort(Object obj[]){
		long start = System.currentTimeMillis();
		sort(obj);
		long end = System.currentTimeMillis();
		System.out.println("time:"+(end - start));
	} 
}
