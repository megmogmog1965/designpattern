package _09_Bridge_01;

import java.util.Arrays;

public class BubbleSortImpl implements SortImpl {

	@Override
	public void sort(Object[] array) {
		// 2.0 sec.
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) { e.printStackTrace(); }

		Arrays.sort(array);
	}
}
