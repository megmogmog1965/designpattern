package _04_FactoryMethod_01;

public class ImagawasCutPrint extends CutPrint {
	@Override
	protected void draw(Cuttable hanzai){
		System.out.println("マンガの絵を描く");
	}
	
	@Override
	protected void cut(Cuttable hanzai){
		System.out.println("彫刻刀を利用して器用に彫る");
	}
	
	@Override
	protected void  print(Cuttable hanzai){
		System.out.println("インクとして、自分の血を使いプリントする");
	}
	
	/**
	 * this is Factory Method for Cuttable.
	 */
	@Override
	protected Cuttable createCuttable(){
		return new Potato();
	}
}