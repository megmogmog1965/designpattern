
public abstract class Teacher{
    public abstract void createStudentList();
    public abstract void callStudents();
}