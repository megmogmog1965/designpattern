package _02_Adapter_01;

public class Teacher{
    public static void main(String args[]){
    	// before.
//        Chairperson chairperson = new Taro();
    	
    	// after.
    	Chairperson chairperson = new TaroAdapter();
    	
        chairperson.organizeClass();
    }
}
