package _12_Decorator_01;

public class Main {

	public static void main(String[] args) {
		Icecream ice1 = new CashewNutsToppingIcecream(new VanillaIcecream());
		Icecream ice2 = new CashewNutsToppingIcecream(new GreenTeaIcecream());
		Icecream ice3 = new SliceAlmondToppingIcecream(new CashewNutsToppingIcecream(new VanillaIcecream()));
		
		System.out.println(ice1.getName());
		System.out.println(ice1.howSweet());
		
		System.out.println(ice2.getName());
		System.out.println(ice2.howSweet());
		
		System.out.println(ice3.getName());
		System.out.println(ice3.howSweet());
	}
}
