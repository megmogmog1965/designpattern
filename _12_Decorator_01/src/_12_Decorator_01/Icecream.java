package _12_Decorator_01;

public interface Icecream {
	
    public String getName();
    
    public String howSweet();
}
