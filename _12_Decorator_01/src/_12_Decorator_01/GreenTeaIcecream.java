package _12_Decorator_01;

public class GreenTeaIcecream implements Icecream {
	
    public String getName() {
        return "抹茶アイスクリーム";
    }
    
    public String howSweet() {
        return "抹茶味";
    }
}
