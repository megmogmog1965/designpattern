package _08_AbstractFactory_01;

import factory.BMWFactory;
import factory.ICarPartsFactory;
import factory.ToyotaFactory;

public class Main {

	public static void main(String args[]) {
		
		// creating TOYOTA car.
		ICarPartsFactory toyotaFactory = new ToyotaFactory();
		Car toyotaCar = new Car(
				toyotaFactory.createTire(), 
				toyotaFactory.createHandle());
		System.out.println(toyotaCar);
		
		
		// creating BMW car.
		ICarPartsFactory bmwFactory = new BMWFactory();
		Car bmwCar = new Car(
				bmwFactory.createTire(), 
				bmwFactory.createHandle());
		System.out.println(bmwCar);
	}
}
