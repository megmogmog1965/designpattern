package _04_FactoryMethod_01;

public class Main {
	public static void main(String args[]) {
		// tanaka.
		System.out.println("==== TANAKA ==========");
		TanakasWoodCutPrint tanaka = new TanakasWoodCutPrint();
		tanaka.createWoodCutPrint();
		
		// imagawa.
		System.out.println("==== IMAGAWA ==========");
		ImagawasCutPrint imagawa = new ImagawasCutPrint();
		imagawa.createWoodCutPrint();
	}
}
