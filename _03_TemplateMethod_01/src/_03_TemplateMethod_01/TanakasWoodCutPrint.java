package _03_TemplateMethod_01;

public class TanakasWoodCutPrint extends WoodCutPrint{
	protected void draw(Cuttable hanzai){
		System.out.println("hanzai にマジックを使って大好きな女の子の絵を描く");
	}

	protected void cut(Cuttable hanzai){
		System.out.println("彫刻刀を使って細部まで丁寧に hanzai を彫る");
	}

	protected void print(Cuttable hanzai){
		System.out.println("版画インクと馬簾を使って豪快にプリントする");
	}
	
	public static void main(String args[]) {
		TanakasWoodCutPrint tanaka = new TanakasWoodCutPrint();
		tanaka.createWoodCutPrint();
	}
}