package water;

/**
 * パッケージ内のみ参照可能な具象クラス
 */
class SugarWater implements Water {
	public int sugar = 0;
	public int water = 0;
	
	public String toString() {
		return "Sugar water (sugar=" + sugar + ", water=" + water  + ").";
	}
}
