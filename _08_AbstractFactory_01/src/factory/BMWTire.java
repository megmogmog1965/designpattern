package factory;

import _08_AbstractFactory_01.Tire;

/**
 * パッケージの外からは見えない具象クラス
 */
class BMWTire implements Tire {
	
	public String toString() {
		return "BMWのタイヤ";
	}
}
