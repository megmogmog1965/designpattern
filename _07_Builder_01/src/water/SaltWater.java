package water;

/**
 * パッケージ内のみ参照可能な具象クラス
 */
class SaltWater implements Water {
	public int salt = 0;
	public int water = 0;
	
	public String toString() {
		return "Salt water (salt=" + salt + ", water=" + water  + ").";
	}
}
