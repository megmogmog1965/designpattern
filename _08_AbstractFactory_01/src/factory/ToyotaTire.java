package factory;

import _08_AbstractFactory_01.Tire;

/**
 * パッケージの外からは見えない具象クラス
 */
class ToyotaTire implements Tire {
	
	public String toString() {
		return "トヨタのタイヤ";
	}
}
