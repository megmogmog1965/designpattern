package paperworks;

public class Paper implements Cloneable {
	private final String shape;
	
	public Paper() {
		this.shape = "Plain paper";
	}
	
	/**
	 * privateなので、外部からは呼べない
	 */
	private Paper(String shape) {
		this.shape = shape;
	}
	
	/**
	 * 時間の掛かる "雪の結晶" Paper のFactory method
	 */
	public Paper cutCrystalShape() {
		String shape = "雪の結晶";
		
		// 作るのに時間がかかる
		for(int i=10 ; i>0 ; i--) {
			System.out.println("雪の結晶の形に切り抜いています... (残り" + i + "秒)");
			
			try { Thread.sleep(1000); } catch (InterruptedException e) {}
		}
		
		System.out.println("完成しました (" + shape + ")");
		
		// やっとできた... ( ´Д｀)=3
		return new Paper(shape);
	}
	
	@Override
	public String toString() {
		return this.shape;
	}
	
	@Override
	public Cloneable createClone() {
		try { Thread.sleep(1000); } catch (InterruptedException e) {}
		
		System.out.println("複製しました (" + shape + ")");
		
		return new Paper(this.shape);
	}
}
