package _05_Singleton_01;

public class Main {
	public static void main(String args[]) {
		// this is error.
//		RegisterNote note = new RegisterNote();
		
		// first time.
		RegisterNote note1 = RegisterNote.getInstance();
		
		// second time.
		RegisterNote note2 = RegisterNote.getInstance();
		
		// same instance.
		assert note1 == note2;
		
		System.out.println("OK!");
	}
}