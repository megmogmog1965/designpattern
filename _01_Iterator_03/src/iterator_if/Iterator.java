package iterator_if;

public interface Iterator {
	public boolean hasNext();
	public Object next();
}