package _13_Visitor_01;

import visitor.OrderCountVisitor;
import element.Order;
import element.OrderItem;
import element.OrderLine;

public class Main {

	public static void main(String[] args) {
		// create orders.
		Order allOrder = createOrder();
		
		// create visitor.
		OrderCountVisitor visitor = new OrderCountVisitor();
		
		// accepts.
		allOrder.accept(visitor);
		
		// show results.
		System.out.println("Order count: " + visitor.getCount());
	}

	public static Order createOrder() {
		// 全商品ライン.
		OrderLine all = new OrderLine();

		// 小口.
		OrderLine line1 = new OrderLine();
		{
			line1.addOrder(new OrderItem(100));
			line1.addOrder(new OrderItem(200));

			OrderLine line1a = new OrderLine();
			{
				line1.addOrder(new OrderItem(10));
				line1.addOrder(new OrderItem(15));
			}

			OrderLine line1b = new OrderLine();
			{
				line1.addOrder(new OrderItem(300));
			}
		}
		all.addOrder(line1);

		// 高額商品?
		OrderLine line2 = new OrderLine();
		{
			line1.addOrder(new OrderItem(19800));
			line1.addOrder(new OrderItem(29800));
		}
		all.addOrder(line2);

		return all;
	}
}
